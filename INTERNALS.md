# Internals

A high level representation of the app's architecture is as follows:

```mermaid
graph LR
  subgraph Server-side
    A[Express Webserver] --> |Request| B[Microservices]
    B -->|Response| A
    D[(MongoDB)]
    A --- D
  end
  subgraph Client-side
    C[AngularJS SPA] --> |Request| A
    A -->|Response| C
  end
```

Communication between client-side code (AngularJS SPA) and Express Webserver relies on HTTP, whereas gRPC is used to communicate with the microservices.

## Server

Written in Node.js with Express and uses a very basic JWT authentication scheme, handled by the `/login` path.  The user sends their credentials (through TLS ideally) and the server responds by storing a JWT in the user's cookies.

To delete the JWT cookie one must send a `/logout` request.

The main API calls are handled through the `/routes` and `/route` paths. The `/routes` path simply returns an array of all the names of the routes for the current user. The `/route` path is responsible for creating, updating and viewing routes. Note that these are privileged paths and a valid JWT is required to communicate through them.

## Client

Comprised of two main pages. The login page which is written entirely in vanilla HTML5/CSS3/JS, and the single-page app which uses AngularJS mixed with a lot of vanilla JS.

The login page is served under the `/` path. The app, and all its  subpaths, is served under the `/app` path.

#### Supported operations:

* Register new account
* Login for existing user
* Create new routes
* Edit existing routes
* View the data and status of existing routes

## Service-SafeRoute

Receives as input the data of a single route, including the route points and the product's temperature and humidity ranges.

Finds the temperature of each route point, at the given date and time, (through OpenWeatherMap API) and calculates whether the route will be safe for the product's preferred range of temperature/humidity or not. In case the route is safe, it still might be too close to the limits of the products preferred conditions. This service adds a warning zone with a certain threshold, to check if a safe route is close to becoming dangerous. The response data includes an `incomplete` parameter which signifies that the data received from external APIs were limited, so the final results might not be 100% correct. For example, the given date and time of some route points might be outside of the supported range of the free OpenWeatherMap API.

All this data, including the temperatures of each point, are sent back to the entity that made the request.