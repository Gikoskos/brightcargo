import grpc from 'grpc';
import * as protoLoader from '@grpc/proto-loader';
import dayjs from 'dayjs';
import { WebApi, Logger } from 'brightcargo-util';


//this is similar to IRoutePointRaw of brightcargo-appdb but
//the date is stringified JSON date instead of a Date object
interface ISafeRoutePoint {
    lat: number;
    lng: number;
    date: string;
    temperature?: number;
    humidity?: number;
}

interface ISafeRouteData {
    incomplete?: boolean;
    is_safe?: boolean;
    warning?: boolean;
    temp_min: number;
    temp_max: number;
    humidity_min: number;
    humidity_max: number;
    points?: ISafeRoutePoint[];
}

interface ISafeRouteServiceParams {
    address: string;
    port: string;
    protopath: string;
    openWeatherMapAPIKey: string;
}

export default class SafeRouteService extends Logger {
    readonly #warningThreshold: number = 1;
    //grpc server hostname
    #hostname: string;
    #packageDef: protoLoader.PackageDefinition;
    #server: grpc.Server;
    #openWeatherMap: WebApi;

    constructor(params: ISafeRouteServiceParams) {
        super('SafeRouteService');
        this.#hostname = `${params.address}:${params.port}`;

        this.#packageDef = protoLoader.loadSync(
            params.protopath,
            {
                keepCase: true,
                defaults: true,
            }
        );
        console.log(`Loaded proto file ${params.protopath}`);

        this.#openWeatherMap = new WebApi('https://api.openweathermap.org/data/2.5/onecall', params.openWeatherMapAPIKey);

        this.#server = new grpc.Server();

        const saferoute_proto = grpc.loadPackageDefinition(this.#packageDef).saferoute;

        this.#server.addService((saferoute_proto as any).SafeRouteService.service, {
            IsRouteSafe: this.IsRouteSafeStub()
        });

        this.#server.bind(this.#hostname, grpc.ServerCredentials.createInsecure());
    }

    private IsRouteSafe = async (request: ISafeRouteData): Promise<ISafeRouteData> => {
        this.log('service called');

        request.incomplete = false;
        request.is_safe = true;
        request.warning = false;

        if (!request.points || !request.points.length) {
            return request;
        }

        let epochSeconds: number,
            dayDiff: number,
            hourDiff: number,
            incompleteCnt: number = 0,
            warning_zone_min: number,
            warning_zone_max: number,
            pointDate: Date,
            data: any;

        for (const routePoint of request.points) {
            pointDate = new Date(JSON.parse(routePoint.date));

            //initialize this routePoint's temp and humidity
            routePoint.temperature = routePoint.humidity = undefined;


            //this whole mess is needed to adapt to openweathermap's free API limitations
            //and additionally limit the number of failed API calls to reduce overhead

            //The difference between today and the route point's date in days.
            //I'm using this information to check which openweathermap API I should use.
            dayDiff = -dayjs().diff(pointDate, 'day');

            //for historical weather data (data that's before the current day) OWM uses a
            //different API than forecast weather data
            if (dayDiff < 0 && dayDiff >= -5 || dayjs().isAfter(pointDate)) {
                epochSeconds = Math.floor(pointDate.getTime() / 1000);
                //the historical weather data provided by OWM only go as far back as 5 days
                try {
                    data = await this.#openWeatherMap.getResource('timemachine', {
                        lat: routePoint.lat.toString(),
                        lon: routePoint.lng.toString(),
                        dt: epochSeconds.toString(),
                        units: 'metric'
                    });

                    routePoint.temperature = data.current.temp;
                    routePoint.humidity = data.current.humidity;
                } catch (err) {
                    incompleteCnt++;
                    this.error(`on IsRouteSafe: this.#openWeatherMap.getResource: ${err.message}`);
                }
            } else if (dayDiff < 7) {
                //the forecast weather data provided by OWM only go as far as 7 days in the future
                try {
                    data = await this.#openWeatherMap.getResource('', {
                        lat: routePoint.lat.toString(),
                        lon: routePoint.lng.toString(),
                        units: 'metric'
                    });

                    if (dayDiff < 2) {
                        //using hour precision for requested weather data that's within the following 48 hours
                        //since OWM provides data for those times
                        hourDiff = dayjs(pointDate).diff(data.current.dt * 1000, 'hour');

                        if (hourDiff < 0) {
                            incompleteCnt++;
                            //this condition should be UNREACHABLE at this point. this guard is used purely to locate possible bugs
                            this.error(`on IsRouteSafe: negative hourDiff: diff(${pointDate.toString()}, ${data.current.dt * 1000})`);
                        } else {
                            routePoint.temperature = data.hourly[hourDiff].temp;
                            routePoint.humidity = data.hourly[hourDiff].humidity;
                        }
                    } else {
                        //using day precision for requested weather data that's beyond the following 2 days
                        routePoint.temperature = data.daily[dayDiff].temp.day;
                        routePoint.humidity = data.daily[dayDiff].humidity;
                    }
                    //@TODO: could add minute precision, if the weather data requested is for times within
                    //60 minutes from the current time
                } catch (err) {
                    this.error(`on IsRouteSafe: this.#openWeatherMap.getResource: ${err.message}`);
                    incompleteCnt++;
                }
            } else {
                this.error(`on IsRouteSafe: datetime out of openWeatherMap data range: (${pointDate.toString()})`);
                incompleteCnt++;
            }

            //this.log(`routepoint temperature: ${routePoint.temperature}`);

            //The following lines are checking whether the route is safe, based on the
            //weather data of each route point that has been acquired earlier

            //the reasoning here is that we only check if this route point is safe for our product IF
            //the route points up to this point were safe as well. This happens because if the route
            //is marked NON-SAFE at a single route point before this one, then it's a waste of resources
            //to check whether it's safe or not for the rest of the route points again (unless we were marking
            //each route point's safety separately and not just the safety of the entire route)
            if (request.is_safe) {
                //if a valid temperature was successfully found for this routepoint at a specific datetime
                if (routePoint.temperature !== undefined) {
                    //if the point's temperature is out of the range of our product's max/min the route is marked
                    //as non-safe
                    if (
                        request.temp_min > routePoint.temperature ||
                        request.temp_max < routePoint.temperature
                    ) {
                        request.is_safe = false;
                    } else if (!request.warning) {
                        //same reasoning as the above check here. If we have a warning marked once, there's
                        //no need to check again
                        warning_zone_min = request.temp_min + this.#warningThreshold;
                        warning_zone_max = request.temp_max - this.#warningThreshold;

                        if (
                            warning_zone_min >= routePoint.temperature ||
                            warning_zone_max <= routePoint.temperature
                        ) {
                            request.warning = true;
                        }
                    }
                }

                if (routePoint.humidity !== undefined) {
                    if (
                        request.humidity_min > routePoint.humidity ||
                        request.humidity_max < routePoint.humidity
                    ) {
                        request.is_safe = false;
                    } else if (!request.warning) {
                        warning_zone_min = request.humidity_min + this.#warningThreshold;
                        warning_zone_max = request.humidity_max - this.#warningThreshold;

                        if (
                            warning_zone_min >= routePoint.humidity ||
                            warning_zone_max <= routePoint.humidity
                        ) {
                            request.warning = true;
                        }
                    }
                }
            }
        }

        request.incomplete = (incompleteCnt != 0);

        return request;
    }

    private IsRouteSafeStub = () => {
        return (call: any, callback: any) => {
            this.IsRouteSafe(call.request)
            .then((response) => {
                callback(null, response);
            });
        };
    }

    public run() {
        console.log(`Running GRPC server for service-saferoute at ${this.#hostname}`);

        this.#server.start();
    }
}
