import path from 'path';
import SafeRouteService from './SafeRouteService';
import { env } from 'process';
import { config } from 'dotenv';


const workspaceRoot = path.join(__dirname, '/../../..');

config({
    path: path.join(workspaceRoot, '.env')
});

const service = new SafeRouteService({
    address: env.SAFEROUTE_ADDRESS,
    port: env.SAFEROUTE_PORT,
    protopath: path.join(workspaceRoot, env.SAFEROUTE_PATH),
    openWeatherMapAPIKey: `appid=${env.OPENWEATHERMAP_APIKEY}`
});

service.run();
