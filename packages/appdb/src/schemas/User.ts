import {
    Document,
    Schema,
} from 'mongoose';
import { IRoute, IRouteRaw, RouteSchema } from './Route';

interface IUserRaw {
    name: string;
    hash: string;
    routes: IRoute[] | IRouteRaw[];
}

interface IUser extends IUserRaw, Document {
    routes: IRoute[];
}

const UserSchema = new Schema<IUser>({
    name: {
        type: String,
        unique: true,
        required: true
    },
    hash: {
        type: String,
        required: true
    },
    routes: {
        type: [ RouteSchema ],
        default: []
    }
});

export {
    IUserRaw,
    IUser,
    UserSchema
};
