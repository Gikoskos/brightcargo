import {
    Document,
    Schema,
} from 'mongoose';
import { IRoutePoint, IRoutePointRaw, RoutePointSchema } from './RoutePoint';

interface IRouteRaw {
    name: string;
    temperature?: [number, number];
    humidity?: [number, number];
    is_safe?: boolean;
    warning?: boolean;
    points?: IRoutePointRaw[] | IRoutePoint[];
}

interface IRoute extends IRouteRaw, Document {
    points?: IRoutePoint[];
}

const RouteSchema = new Schema<IRoute>({
    name: {
        type: String,
        required: true,
        unique: true,
        sparse: true,
        default: '',
    },
    temperature: [
        { type: Number },
        { type: Number }
    ],
    humidity: [
        { type: Number },
        { type: Number }
    ],
    is_safe: Boolean,
    warning: Boolean,
    points: [ RoutePointSchema ]
});

export {
    IRouteRaw,
    IRoute,
    RouteSchema
};
