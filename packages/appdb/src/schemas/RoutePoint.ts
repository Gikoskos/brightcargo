import {
    Document,
    Schema,
} from 'mongoose';

interface IRoutePointRaw {
    lat: number;
    lng: number;
    date: Date;
    temperature?: number;
    humidity?: number;
}

interface IRoutePoint extends IRoutePointRaw, Document {}

const RoutePointSchema = new Schema<IRoutePoint>({
    lat: {
        type: Number,
        required: true
    },
    lng: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    temperature: {
        type: Number,
        required: false
    },
    humidity: {
        type: Number,
        required: false
    }
});

export {
    IRoutePointRaw,
    IRoutePoint,
    RoutePointSchema
};
