import mongoose, {
    Connection,
    Model,
    model,
    ClientSession
} from 'mongoose';
import { Logger } from 'brightcargo-util';
import { IRoutePoint, RoutePointSchema } from './schemas/RoutePoint';
import { IRoute, RouteSchema } from './schemas/Route';
import { IUser, UserSchema } from './schemas/User';

const dbOpts: mongoose.ConnectionOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
};

export default class AppDb extends Logger {
    #db: Connection;
    #initialized: boolean = false;
    public readonly User: Model<IUser>;
    public readonly Route: Model<IRoute>;
    public readonly RoutePoint: Model<IRoutePoint>;

    constructor(conn_string: string) {
        super('AppDb', conn_string);

        mongoose.connect(conn_string, dbOpts, (err) => {
            if (err) {
                this.error(err.message);
                throw new Error(err.message);
            }
            this.#initialized = true;
            //this.#db.dropDatabase();
        });

        this.#db = mongoose.connection;

        this.#db.on('error', this.onDbErr);

        this.#db.once('open', () => this.log('Connected successfully'));

        this.User = model<IUser>('User', UserSchema);
        this.Route = model<IRoute>('Route', RouteSchema);
        this.RoutePoint = model<IRoutePoint>('RoutePoint', RoutePointSchema);
    }

    public async startSession(): Promise<ClientSession> {
        if (!this.#initialized) {
            this.error('startSession called on an uninitialized connection');

            throw new Error('Can\'t start session on uninitialized connection.');
        }

        const session = await this.#db.startSession();

        return session;
    }

    private onDbErr = (err: any[]) => {
        if (err) {
            this.error('' + err);
        } else {
            this.error('Unspecified');
        }
    }
}

export {
    IRoutePoint, RoutePointSchema,
    IRoute, RouteSchema,
    IUser, UserSchema
};

export { IRoutePointRaw } from './schemas/RoutePoint';
export { IRouteRaw } from './schemas/Route';
export { IUserRaw } from './schemas/User';
