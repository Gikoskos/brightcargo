import { Request, Response, NextFunction } from 'express';

export default function InjectDependency<T>(dependency: T, name: string) {
    return function (req: Request, res: Response, next: NextFunction) {
        res.locals[name] = dependency;
        next();
    }
}