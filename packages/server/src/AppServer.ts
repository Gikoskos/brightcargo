import express, {
    Application,
} from 'express';
import cookieParser from 'cookie-parser';
import helmet from 'helmet';
import InjectDependency from './util/InjectDependency';
import { Logger } from 'brightcargo-util';

export default class AppServer extends Logger {
    public readonly app: Application;
    #port: number;
    #name: string;

    constructor(name: string, port: number, publicdir: string) {
        super('AppServer', name);
        this.app = express();

        this.#port = port;
        this.#name = name;

        this.app.use(express.json());
        this.app.use(cookieParser());
        this.app.use(helmet());
        this.app.use(InjectDependency(this.#name, 'domain'));

        this.app.use(express.static(publicdir, { index: false }));
        this.log('Initialized');
    }

    public run() {
        this.app.listen(this.#port, err => {
            if (err)
                return this.error(err);
            return this.log(`Listening on ${this.#port}`);
        });
    }

    public get port() {
        return this.#port;
    }
}
