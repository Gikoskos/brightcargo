import { Request, Response } from 'express';
import AuthenticationService from '../services/AuthenticationService';
import TokenService from '../services/TokenService';

const isValidLoginRequest = (body: any) : boolean => {
    if (
        body &&
        body.user && body.pass &&
        typeof body.user == 'string' &&
        typeof body.pass == 'string'
       ) {
        return true;
    }

    return false;
}

/**
 * Authenticates the user based on the provided credentials and
 * returns a JWT which can be used on requests that require authorization.
 * @param {AuthenticationService} res.locals.authenService
 * @param {TokenService} res.locals.tokenService 
 */
export default async function LoginController(req: Request, res: Response) {
    if (!isValidLoginRequest(req.body)) {
        res.sendStatus(400);
    } else {
        const authenService = res.locals.authenService as AuthenticationService,
            tokenService = res.locals.tokenService as TokenService;

        if (!authenService || !tokenService) {
            console.error('LoginController: Inject AuthenticationService/TokenService dependencies');
            return res.sendStatus(500);
        }

        let value = await authenService.authenticate(req.body.user, req.body.pass);

        if (value) {
            if (value == 'Internal error') {
                return res.sendStatus(500);
            } else {
                return res.status(401).send(value);
            }
        } else {
            const token = tokenService.sign(req.body.user);

            return res
            .cookie('access-token', token, {
                //secure: true,
                httpOnly: true,
                sameSite: 'strict'
            })
            .sendStatus(200);
        }
    }
}
