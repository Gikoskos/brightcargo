import { Request, Response, NextFunction } from 'express';
import TokenService from '../services/TokenService';
import AppDb, { IUser } from 'brightcargo-appdb';


/**
 * Checks whether the user is authorized to perform the given request.
 * Should be called before any path that should be accessed only by authorized users.
 * @param {TokenService} res.locals.tokenService 
 * @param {AppDb} res.locals.db 
 */
export default async function AuthorizeRequest(
    req: Request,
    res: Response,
    next: NextFunction
) {
    const tokenService = res.locals.tokenService as TokenService;
    const db = res.locals.db as AppDb;

    if (!tokenService) {
        console.error('AuthorizeRequest: Inject TokenService dependency');
        return res.sendStatus(500);
    }

    if (!db) {
        console.error('AuthorizeRequest: Inject AppDb dependency');
        return res.sendStatus(500);
    }

    //the session token is provided through the cookies included by the request
    let token: string = req.cookies['access-token'];

    if (!token) {
        return res.status(401).send('Unauthorized: Session cookie not found');
    }

    //check if the JWT is valid, and decode it
    const decodedToken = tokenService.validate(token);

    if (!decodedToken) {
        return res.sendStatus(401);
    }

    let userDoc: IUser;

    try {
        //check whether the user mentioned in the JWT's data
        //is a valid registered user
        userDoc = await db.User.findOne({
            name: decodedToken.username
        }).exec();
    } catch (err) {
        console.error(`on AuthorizeRequest: db.User.findOne: ${err.message}`);
        return res.sendStatus(500);
    }

    //if the user does not exist, redirect to the logout path which
    //deletes the cookie
    if (!userDoc) {
        return res.redirect('../logout');
    }

    //inject the username string so that the next controllers can know
    //which user made the request
    res.locals.username = decodedToken.username;

    next();
}