import { Request, Response } from 'express';
import AuthenticationService from '../services/AuthenticationService';

const isValidRegisterRequest = (body: any) : boolean => {
    if (
        body &&
        body.user && body.pass &&
        typeof body.user == 'string' &&
        typeof body.pass == 'string'
       ) {
        return true;
    }

    return false;
}

/**
 * Registers a user with the provided credentials, if they don't already exist
 * in the database.
 * @param {AuthenticationService} res.locals.authenService 
 */
export default async function RegisterController(req: Request, res: Response) {
    if (!isValidRegisterRequest(req.body)) {
        res.sendStatus(400);
    } else {
        const authenService = res.locals.authenService as AuthenticationService;

        if (!authenService) {
            console.error('RegisterController: Inject AuthenticationService dependency');
            return res.sendStatus(500);
        }

        let value = await authenService.register(req.body.user, req.body.pass);

        if (value) {
            if (value == 'Internal error') {
                return res.sendStatus(500);
            } else {
                return res.status(401).send(value);
            }
        } else {
            return res.sendStatus(200);
        }
    }
}