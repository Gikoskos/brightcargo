import { Request, Response } from 'express';
import AppDb, { IUser } from 'brightcargo-appdb';

export default async function RoutesController(req: Request, res: Response) {
    const db: AppDb = res.locals.db;
    const username: string = res.locals.username;
    let userDoc: IUser;
    let routeNames = [];

    if (!username) {
        console.error('RoutesController: Inject AuthorizeRequest dependency');
        return res.sendStatus(500);
    }

    if (!db) {
        console.error('RoutesController: Inject AppDb dependency');
        return res.sendStatus(500);
    }

    try {
        userDoc = await db.User.findOne({
            name: username
        }).exec();
    } catch (err) {
        console.error(`on _RoutesController: db.User.findOne: ${err.message}`);
        return res.sendStatus(500);
    }

    if (!userDoc) {
        return res.status(400).send(`Couldn't find user with that name.`);
    }

    for (const route of userDoc.routes) {
        routeNames.push(route.name);
    }

    res.status(200).json(routeNames);
}