import { Request, Response } from 'express';

/**
 * Deletes the 'access-token' cookie.
 */
export default function LogoutController(req: Request, res: Response) {
    res
    .cookie('access-token', '', {
        maxAge: 0,
        sameSite: 'strict'
    }).sendStatus(200);
}
