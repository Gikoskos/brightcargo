import { Request, Response } from 'express';
import path from 'path';

/**
 * Returns the main app page under the /app route.
 * 
 * @param {string} req.locals.staticFilesPath - Path of the app's static files (including
 * stylesheets, html pages etc). It is injected as a dependency.
 */
export default function AppController(req: Request, res: Response) {
    res.sendFile(path.join(res.locals.staticFilesPath, 'app.html'));
}