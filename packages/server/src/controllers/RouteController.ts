import { Request, Response } from 'express';
import SafeRouteService, { ISafeRouteData } from '../services/SafeRouteService';
import AppDb, {
    IRoute,
    IUser,
    IRouteRaw,
    IRoutePointRaw
} from 'brightcargo-appdb';


const isValidRouteRequest = (body: any) : boolean => {
    if (
        body &&
        body.query &&
        typeof body.query == 'string' &&
        typeof body.route == 'object' &&
        typeof body.route.name == 'string'
    ) {
        const { route } = body;

        if (
            Array.isArray(route.temperature) &&
            route.temperature.length == 2 &&
            Array.isArray(route.humidity) &&
            route.humidity.length == 2 &&
            Array.isArray(route.points)
        ) {
            for (const temp of route.temperature) {
                if (typeof temp != 'number') {
                    return false;
                }
            }

            for (const hum of route.humidity) {
                if (typeof hum != 'number') {
                    return false;
                }
            }

            const { points } = route;

            for (const point of points) {
                if (typeof point != 'object') {
                    return false;
                }

                if (typeof point.lat != 'number') {
                    return false;
                }

                if (typeof point.lng != 'number') {
                    return false;
                }

                if (typeof point.date != 'string') {
                    return false;
                }
            }
        }

        return true;
    }

    return false;
}

/**
 * Creates a new route for the {@param res.locals.username} user with
 * the {@param routeData.name} name.
 * Note that this operation simply creates a route with a specific name, but
 * doesn't add any more data to the newly created route.
 * To update a specific route's data, the 'update' operation should be used.
 *
 * @param routeData
 * @param {string} res.locals.username
 * @param {AppDb} res.locals.db
 */
async function createRoute(routeData: IRouteRaw, res: Response): Promise<Response<any>> {
    const db: AppDb = res.locals.db;
    let session;
    let userDoc: IUser;
    let newRouteName = routeData.name.trim();

    if (newRouteName == '') {
        return res.status(400).send(`Empty route names are not allowed.`);
    }

    try {
        session = await db.startSession();
    } catch (err) {
        console.error(`on createRoute: startSession: ${err.message}`);
        return res.sendStatus(500);
    }

    session.startTransaction();

    try {
        userDoc = await db.User.findOne({
            name: res.locals.username
        }).session(session).exec();
    } catch (err) {
        session.endSession(session);
        console.error(`on createRoute: db.User.findOne: ${err.message}`);
        return res.sendStatus(500);
    }

    if (!userDoc) {
        session.endSession(session);
        return res.status(400).send(`Couldn't find user with that name.`);
    }

    const newRoute = new db.Route({
        name: newRouteName,
        temperature: [0, 0],
        humidity: [0, 0]
    });

    if (!userDoc.routes) {
        userDoc.routes = [ newRoute ];
    } else {
        if (userDoc.routes.some((route) => route.name == newRouteName)) {
            session.endSession(session);
            return res.status(400).send('Route with the same name already exists.');
        }

        userDoc.routes.push(newRoute);
    }

    try {
        await userDoc.save({ session });
        await session.commitTransaction();
    } catch (err) {
        console.error(`on createRoute: userDoc.save: ${err.message}`);
        session.endSession(session);
        return res.sendStatus(500);
    }

    session.endSession(session);
    return res.sendStatus(200);
}

/**
 * Updates and processes a route's data and sends it back as a response.
 * The response includes the route's safety information based on the new data.
 * Check the 'infoRoute' function to see the format of that response.
 *
 * Note that the route must already have been created with a 'create'
 * operation. If the route doesn't exist, this query will fail.
 *
 * @param routeData 
 * @param {string} res.locals.username
 * @param {AppDb} res.locals.db
 * @param {SafeRouteService} res.locals.safeRouteService
 */
async function updateRoute(routeData: IRouteRaw, res: Response): Promise<Response<any>> {
    const db: AppDb = res.locals.db;
    const safeRouteService: SafeRouteService = res.locals.safeRouteService;
    let session;
    let userDoc: IUser;
    let serviceData: ISafeRouteData = {};

    //sanitize input
    if (routeData.temperature[0] > routeData.temperature[1]) {
        return res.status(400).send('Min temperature should be less than max temperature.');
    }

    if (
        routeData.humidity[0] < 0 || routeData.humidity[0] > 100 ||
        routeData.humidity[1] < 0 || routeData.humidity[1] > 100
    ) {
        return res.status(400).send('Humidity is measured by percentage so legal values are between 0 and 100.');
    }

    if (routeData.humidity[0] > routeData.humidity[1]) {
        return res.status(400).send('Min humidity should be less than max humidity.');
    }

    try {
        session = await db.startSession();
    } catch (err) {
        console.error(`on createRoute: startSession: ${err.message}`);
        return res.sendStatus(500);
    }

    session.startTransaction();

    try {
        userDoc = await db.User.findOne({
            name: res.locals.username
        }).session(session).exec();
    } catch (err) {
        console.error(`on updateRoute: db.User.findOne: ${err.message}`);
        session.endSession();
        return res.sendStatus(500);
    }

    if (!userDoc) {
        session.endSession();
        return res.status(400).send(`Couldn't find user with that name.`);
    }

    if (!userDoc.routes) {
        session.endSession();
        return res.status(400).send(`Couldn't find route with that name for the current user.`);
    }

    const updatedRoute = userDoc.routes.find((route: IRoute) => route.name === routeData.name);

    if (!updatedRoute) {
        session.endSession();
        return res.status(400).send(`Couldn't find route with that name for the current user.`);
    }

    //The following section calculates the route's safety based on the updated route data

    //first prepare the service's request data
    serviceData.humidity_min = routeData.humidity[0];
    serviceData.humidity_max = routeData.humidity[1];
    serviceData.temp_min = routeData.temperature[0];
    serviceData.temp_max = routeData.temperature[1];

    serviceData.points = [];

    for (const point of routeData.points) {
        serviceData.points.push({
            lat: point.lat,
            lng: point.lng,
            date: JSON.stringify(point.date),
        });
    }

    //make the request to the service
    try {
        serviceData = await safeRouteService.IsRouteSafe(serviceData);
    } catch (err) {
        console.error(`on infoRoute: safeRouteService.IsRouteSafe: `, err);
        session.endSession();
        return res.sendStatus(500);
    }

    //now store the new data on the database and also on the response object
    //that the user will receive
    updatedRoute.temperature = routeData.temperature;
    updatedRoute.humidity = routeData.humidity;
    updatedRoute.is_safe = routeData.is_safe = serviceData.is_safe;
    updatedRoute.warning = routeData.warning = serviceData.warning;

    updatedRoute.points = [];

    for (let i = 0; i < serviceData.points.length; i++) {
        const servicePoint = serviceData.points[i];

        routeData.points[i].temperature = servicePoint.temperature;
        routeData.points[i].humidity = servicePoint.humidity;

        updatedRoute.points.push(new db.RoutePoint({
            ...servicePoint,
            date: routeData.points[i].date
        }));
    }

    try {
        await userDoc.save({ session });
        await session.commitTransaction();
    } catch (err) {
        console.error(`on updateRoute: userDoc.save: ${err.message}`);
        session.endSession();
        return res.sendStatus(500);
    }

    session.endSession();
    return res.status(200).send(routeData);
}

/**
 * Gets the route's data from the database and sends it back as a response.
 * The response is a JSON with the following format:
 *
 *  {
 *      "name": string,
 *      "temperature": [number, number],
 *      "humidity": [number, number],
 *      "points": {
 *          "lat": number,
 *          "lng": number,
 *          "date": Date
 *      }[] | [],
 *      "is_safe": boolean,
 *      "warning": boolean
 *  }
 *
 * @param routeData 
 * @param {string} res.locals.username
 * @param {AppDb} res.locals.db
 */
async function infoRoute(routeData: IRouteRaw, res: Response): Promise<Response<any>> {
    const db: AppDb = res.locals.db;
    let userDoc: IUser;

    try {
        userDoc = await db.User.findOne({
            name: res.locals.username
        }).exec();
    } catch (err) {
        console.error(`on updateRoute: db.User.findOne: ${err.message}`);
        return res.sendStatus(500);
    }

    if (!userDoc) {
        return res.status(400).send(`Couldn't find user with that name.`);
    }

    if (!userDoc.routes) {
        return res.status(400).send(`Couldn't find route with that name for the current user.`);
    }

    const selectedRoute = userDoc.routes.find((route: IRoute) => route.name === routeData.name);

    if (!selectedRoute) {
        return res.status(400).send(`Couldn't find route with that name for the current user.`);
    }

    routeData.temperature = selectedRoute.temperature;
    routeData.humidity = selectedRoute.humidity;
    routeData.points = [];

    //if no points are registered for the selected route
    if (!selectedRoute.points || !selectedRoute.points.length) {
        routeData.is_safe = true;
        routeData.warning = false;

        //return the data
        return res.status(200).send(routeData);
    }

    //copy the route's data on the response object and send it back
    for (const point of selectedRoute.points) {
        (routeData.points as IRoutePointRaw[]).push({
            lat: point.lat,
            lng: point.lng,
            date: point.date,
            temperature: point.temperature,
            humidity: point.humidity
        });
    }

    routeData.is_safe = selectedRoute.is_safe;
    routeData.warning = selectedRoute.warning;

    return res.status(200).send(routeData);
}

/**
 * Controller that handles operations performed on routes.
 * A valid body for a Route request is a JSON with the following format:
 * 
 *  {
 *      "query": string,
 *      "route": {
 *          "name": string,
 *          "temperature": [number, number],
 *          "humidity": [number, number],
 *          "points": {
 *              "lat": number,
 *              "lng": number,
 *              "date": Date
 *          }[] | []
 *      }
 *  }
 * 
 * ALL fields must be included in the requests, even if they have values
 * that will be ignored (in the case of an 'info' query, for example).
 * 
 * Note that the 'points' field can also be an empty array.
 *
 * A standard query flow for creating a new route, adding data and getting
 * back information on the newly created route, should be
 *
 * 'create' -> 'update' -> 'info'
 *
 * For more details, check the operation handlers.
 */
export default async function RouteController(req: Request, res: Response) {
    const { body } = req;

    if (!isValidRouteRequest(body)) {
        return res.sendStatus(400);
    }

    const routeData: IRouteRaw = body.route;

    switch (body.query) {
    case 'create':
        return createRoute(routeData, res);
    case 'update':
        return updateRoute(routeData, res);
    case 'info':
        return infoRoute(routeData, res);
    }

    return res.status(400).send('Invalid query.');
}