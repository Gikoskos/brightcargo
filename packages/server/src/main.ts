import path from 'path';
import { env } from 'process';
import { config } from 'dotenv';
import { Request, Response } from 'express';
import AppDb from 'brightcargo-appdb';
import AppServer from './AppServer';
import AuthenticationService from './services/AuthenticationService';
import TokenService from './services/TokenService';
import LoginController from './controllers/LoginController';
import LogoutController from './controllers/LogoutController';
import RegisterController from './controllers/RegisterController';
import SafeRouteService from './services/SafeRouteService';
import InjectDependency from './util/InjectDependency';
import AuthorizeRequest from './controllers/AuthorizeRequest';
import AppController from './controllers/AppController';
import RouteController from './controllers/RouteController';
import RoutesController from './controllers/RoutesController';


//First thing is to define all the paths for external resources used by the server.
//This makes it easy to change them dynamically in the future (in case I make a more
//complete production build that builds all packages under a single build folder and relies
//on a different root path, rather than the yarn workspace root)
const workspaceRoot = path.join(__dirname, '/../../..');

config({
    path: path.join(workspaceRoot, '.env')
});


const staticFilesPath = path.join(workspaceRoot, env.STATIC_FILES_PATH);
const safeRouteServiceHost = `${env.SAFEROUTE_ADDRESS}:${env.SAFEROUTE_PORT}`;
const protopath = path.join(workspaceRoot, env.SAFEROUTE_PATH);


//Initialize DB
const db = new AppDb(env.CONNECTION_STRING);

//Initialize web server object
const server = new AppServer('BrightCargo', Number(env.SERVER_PORT), staticFilesPath);

//Initialize services
const authenService = new AuthenticationService(db.User);
const tokenService = new TokenService(env.SECRET_KEY);
const safeRouteService = new SafeRouteService(safeRouteServiceHost, protopath);


//Define routes
//For more details on how the routes work, read the comments on their
//respective controllers
server.app.get('/', (req: Request, res: Response) => {
    res.sendFile(path.join(staticFilesPath, 'login.html'));
});

server.app.post(
    '/login',
    InjectDependency(authenService, 'authenService'),
    InjectDependency(tokenService, 'tokenService'),
    InjectDependency(db, 'db'),
    LoginController
);

server.app.get(
    '/logout',
    LogoutController
);

server.app.post(
    '/register',
    InjectDependency(authenService, 'authenService'),
    InjectDependency(db, 'db'),
    RegisterController
);

//The following paths can only be accessed by authorized users.
//In the context of this app, the users who have access are all users
//who have a valid session token and a document entry in the app's database.
server.app.get(
    '/app',
    InjectDependency(tokenService, 'tokenService'),
    InjectDependency(db, 'db'),
    AuthorizeRequest,
    InjectDependency(staticFilesPath, 'staticFilesPath'),
    AppController
);

server.app.post(
    '/route',
    InjectDependency(tokenService, 'tokenService'),
    InjectDependency(db, 'db'),
    AuthorizeRequest,
    InjectDependency(safeRouteService, 'safeRouteService'),
    RouteController
);

server.app.get(
    '/routes',
    InjectDependency(tokenService, 'tokenService'),
    InjectDependency(db, 'db'),
    AuthorizeRequest,
    RoutesController
);

server.run();
