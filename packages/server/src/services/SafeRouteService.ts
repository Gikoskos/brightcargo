import grpc from 'grpc';
import * as protoLoader from '@grpc/proto-loader';
import { Logger } from 'brightcargo-util';

//@TODO: add these type definitions to a shared point to avoid code repetition
interface ISafeRoutePoint {
    lat: number;
    lng: number;
    date: string;
    temperature?: number;
    humidity?: number;
}

export interface ISafeRouteData {
    incomplete?: boolean;
    is_safe?: boolean;
    warning?: boolean;
    temp_min?: number;
    temp_max?: number;
    humidity_min?: number;
    humidity_max?: number;
    points?: ISafeRoutePoint[];
}

type IIsRouteSafeCallback = (err: any, response: ISafeRouteData) => void;

interface ISafeRouteServiceClient extends grpc.Client {
    IsRouteSafe: (
        request: ISafeRouteData,
        callback: IIsRouteSafeCallback
    ) => void;
}

export default class SafeRouteService extends Logger {
    #serviceHost: string;
    #packageDef: protoLoader.PackageDefinition;
    #client: ISafeRouteServiceClient = null;
    #saferouteProto: any;

    constructor(serviceHost: string, protoPath: string) {
        super('SafeRouteService', serviceHost);

        this.#serviceHost = serviceHost;

        this.#packageDef = protoLoader.loadSync(
            protoPath,
            {
                keepCase: true,
                defaults: true,
            }
        );

        this.#saferouteProto = grpc.loadPackageDefinition(this.#packageDef).saferoute;

        this.#client = new this.#saferouteProto.SafeRouteService(
            this.#serviceHost,
            grpc.credentials.createInsecure()
        );

        this.log(`Bound client at ${this.#serviceHost}`);
    }

    public async IsRouteSafe(request: ISafeRouteData): Promise<ISafeRouteData> {
        return new Promise((resolve, reject) => {
            this.#client.IsRouteSafe(request, (err, response) => {
                if (err) {
                    return reject(err);
                }

                resolve(response);
            });
        });
    }
}