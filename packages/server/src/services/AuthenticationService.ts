import { Model } from 'mongoose';
import { IUser } from 'brightcargo-appdb';
import bcrypt from 'bcryptjs';
import { Logger } from 'brightcargo-util';


export default class AuthenticationService extends Logger {
    #User: Model<IUser>;

    constructor(userModel: Model<IUser>) {
        super('AuthenticationService');
        this.#User = userModel;
    }

    public async register(username: string, pass: string): Promise<string | null> {
        let userData: IUser;
        
        try {
            userData = await this.#User.findOne({
                name: username,
            }).exec();
        } catch (err) {
            //check if error is returned in case the document isn't found too (thanks mongoose docs)
            this.error(err.message);
            return 'Internal error';
        }

        if (userData) {
            return 'User with that name already exists.';
        }

        let newUserHash: string;

        try {
            newUserHash = await bcrypt.hash(pass, 10);
        } catch (err) {
            if (err && err.message) {
                this.error(`on register: bcrypt.hash: ${err.message}`);
            } else {
                this.error('on register: bcrypt.hash');
            }
            return 'Internal error';
        }

        try {
            await this.#User.create({
                name: username,
                hash: newUserHash,
                routes: []
            });
        } catch (err) {
            this.error(`on register: this.#User.create: ${err.message}`);
            return 'Internal error';
        }

        return null;
    }

    public async authenticate(username: string, pass: string): Promise<string | null> {
        let userData: IUser;
        
        try {
            userData = await this.#User.findOne({
                name: username
            }).exec();
        } catch (err) {
            this.error(err.stack);
            return 'Internal error';
        }

        if (!userData) {
            return 'User not found';
        }

        let validPassword;

        try {
            validPassword = await bcrypt.compare(pass, userData.hash);
        } catch (err) {
            this.error('login: bcrypt.compare');
            return 'Internal error';
        }

        if (!validPassword) {
            return 'Invalid password';
        }

        return null;
    }
}
