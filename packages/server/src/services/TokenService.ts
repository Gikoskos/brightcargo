import { Logger } from 'brightcargo-util';
import jwt from 'jsonwebtoken';

interface ITokenData {
    username: string;
}

export default class TokenService extends Logger {
    #privateKey: string;

    constructor(privateKey: string) {
        super('TokenService');

        this.#privateKey = privateKey;
    }

    public sign(username: string): string {
        let token = jwt.sign({ username }, this.#privateKey);

        return token;
    }

    public validate(token: string): ITokenData | null {
        try {
            let decoded = jwt.verify(token, this.#privateKey) as ITokenData;

            return decoded;
        } catch (err) {
            this.log(`invalid token ${token}`);
        }

        return null;
    }
}