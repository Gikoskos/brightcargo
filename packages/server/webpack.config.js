const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = {
    mode: 'production',
    target: 'node',
    entry: './src/main.ts',
    node: {
        global: false,
        __filename: false,
        __dirname: false,
    },
    externals: [
        nodeExternals({
            modulesDir: path.resolve(__dirname, '../../node_modules'),
            allowlist: [ /^brightcargo-/ ]
        }),
    ],
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },
    devtool: 'source-map',
    resolve: {
        extensions: [ '.ts', '.js' ],
    },
    output: {
        filename: 'brightcargo-server.js',
        path: path.resolve(__dirname, 'dist'),
    },
};