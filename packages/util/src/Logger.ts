export class Logger {
    #derived: String;
    #name: String;

    constructor(owner: String, name?: String) {
        this.#derived = owner;
        this.#name = (name) ? name : '';
    }

    protected log = (msg: String) => {
        console.log(`${this.#derived} '${this.#name}' log: ${msg}`);
    }

    protected error = (msg: String) => {
        console.error(`${this.#derived} '${this.#name}' error: ${msg}`);
    }
}