import fetch from 'node-fetch';
import { Logger } from './Logger';

interface IWebApiParam {
    [index: string]: string;
}

export class WebApi extends Logger {
    #hostname: string;
    #apiKey: string;
    #enableLogging: boolean;

    constructor(hostname: string, apikey: string, enableLogging: boolean = false) {
        super('WebApi', hostname);

        this.#hostname = hostname;
        this.#apiKey = apikey;
        this.#enableLogging = enableLogging;
    }

    get hostname() {
        return this.#hostname;
    }

    get apiKey() {
        return this.#apiKey;
    }

    async getResource(endpoint: string, params: IWebApiParam) {
        if (endpoint.length && endpoint.charAt(0) != '/') {
            endpoint = `/${endpoint}`;
        }

        endpoint += `?${this.apiKey}`;

        for (const paramName in params) {
            endpoint += `&${paramName}=${params[paramName]}`;
        }

        const requestURL = `${this.hostname}${endpoint}`;
        let response, data;

        try {
            response = await fetch(requestURL);
        } catch (error) {
            if (this.#enableLogging) {
                this.error(`WebApi(${this.#hostname}):getResource: network error while fetching ${requestURL}`);
            }

            if (error && error.message) {
                throw new Error(error.message);
            }

            throw new Error(`Network error while trying to reach ${this.#hostname}`);
        }

        try {
            data = await response.json();
        } catch (err) { }

        if (!response.ok) {
            if (this.#enableLogging) {
                this.error(`WebApi(${this.#hostname}):getResource: abnormal status code while fetching ${requestURL}`);
            }

            if (data && data.message) {
                throw new Error(data.message);
            }

            throw new Error(`Abnormal status code while trying to reach ${this.#hostname}`);
        }

        return data;
    }
}