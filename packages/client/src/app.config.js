angular
.module('brightcargoApp')
.config([
    '$routeProvider',
    function config($routeProvider) {
        $routeProvider
        .when('/list', {
            templateUrl: 'templates/list.html',
            controller: [
                '$rootScope',
                '$scope',
                '$http',
                ListController
            ]
        })
        .when('/route/:routeId', {
            templateUrl: 'templates/route.html',
            controller: [
                '$rootScope',
                '$scope',
                '$http',
                '$routeParams',
                '$window',
                '$filter',
                RouteController
            ]
        })
        .otherwise('/list');
    }
])
.run([
    '$http',
    '$rootScope',
    function ($http, $rootScope) {
        $rootScope.routeNames = [];
        $rootScope.username = localStorage.getItem('user');

        $http.get('/routes', {
            withCredentials: true,
        })
        .then(
            (response) => $rootScope.routeNames = $rootScope.routeNames.concat(response.data),
            (error) => console.log(error.statusText) //modalNotify('Failed getting routes', `${error.statusText}`)
        );    
    
    }
]);