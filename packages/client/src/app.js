angular.module('brightcargoApp', ['ngRoute', 'angular-flatpickr']);

function modalNotify(title, text) {
    const box = document.getElementById('notification-box'),
        boxTitle = document.getElementById('notification-box-title'),
        boxText = document.getElementById('notification-box-text');

    boxTitle.textContent = title;
    boxText.textContent = text;

    box.checked = true;
}

function makeRouteRequest(
    query = '',
    routeName = '',
    tempMin = 0,
    tempMax = 0,
    humMin = 0,
    humMax = 0,
    points = []
) {
    const body = {
        query,
        route: {
            name: routeName,
            temperature: [tempMin, tempMax],
            humidity: [humMin, humMax],
            points
        }
    };

    if (points.length !== 0) {
        body.route.points = points.map((routePoint) => ({
            lat: routePoint.lat,
            lng: routePoint.lng,
            date: routePoint.date
        }));
    }

    return JSON.stringify(body);
}

function enableBackButton() {
    const backAnchor = document.getElementById('header-back-anchor');
    const backBtn = document.getElementById('header-back-btn');

    backAnchor.classList.toggle('disable-link', false);
    backBtn.classList.toggle('shadowed', true);
    backBtn.disabled = false;
}

function disableBackButton() {
    const backAnchor = document.getElementById('header-back-anchor');
    const backBtn = document.getElementById('header-back-btn');

    backBtn.classList.toggle('shadowed', false);
    backAnchor.classList.toggle('disable-link', true);
    backBtn.disabled = true;
}

(function () {
    const logoutBtn = document.getElementById('header-logout-btn');

    logoutBtn.addEventListener('click', function () {
        localStorage.removeItem('user');
        fetch('/logout', {
            method: 'GET',
            credentials: 'include'
        }).then(() => {
            window.location.href = '/';
        }, (err) => {
            if (err) {
                if (err.message) {
                    modalNotify('Error!', err.message);
                } else {
                    modalNotify('Error!', err);
                }
            } else {
                modalNotify('Unexpected error!', 'Failed connecting to server.');
            }
        });
    });
}());
