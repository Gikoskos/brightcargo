async function getRouteInfo($scope, $http, routeSaveBtn, infoCardSpinner, infoCardMessage) {
    routeSaveBtn.disabled = true;
    infoCardMessage.classList.add('hidden');
    infoCardSpinner.classList.remove('hidden');
    infoCardMessage.classList.remove('dangerous-status-message');
    infoCardMessage.classList.remove('warning-status-message');
    infoCardMessage.classList.remove('safe-status-message');

    const requestData = makeRouteRequest('info', $scope.routeName);

    $scope.routeStatus = '';

    try {
        let result = (await $http.post('/route', requestData, {
            withCredentials: true,
        })).data;

        if (!result.is_safe) {
            $scope.routeStatus = 'DANGEROUS';
            infoCardMessage.classList.add('dangerous-status-message');
        } else if (result.warning) {
            $scope.routeStatus = 'WARNING';
            infoCardMessage.classList.add('warning-status-message');
        } else {
            $scope.routeStatus = 'SAFE';
            infoCardMessage.classList.add('safe-status-message');
        }

        $scope.humidity = {
            min: result.humidity[0],
            max: result.humidity[1]
        };

        $scope.temperature = {
            min: result.temperature[0],
            max: result.temperature[1]
        };

        $scope.routePoints = [];

        for (const point of result.points) {
            $scope.routePoints.push({
                id: $scope.nextId++,
                lat: point.lat,
                lng: point.lng,
                date: new Date(point.date)
            });
        }
    } catch (err) {
        console.error(err);
        if (err.status == 400) {
            modalNotify('Invalid route data!', `${err.data}`);
        } else {
            modalNotify('Something happened!', `Status ${err.status}: ${err.statusText}`);
        }

        $scope.routeStatus = 'Error processing data.';
    }

    routeSaveBtn.disabled = false;
    infoCardSpinner.classList.add('hidden');
    infoCardMessage.classList.remove('hidden');
    $scope.$apply();
}

function RouteController($rootScope, $scope, $http, $routeParams, $window, $filter) {
    const routeNameList = document.getElementById('route-point-list');
    const routeSaveBtn = document.getElementById('save-route-btn');
    const infoCardSpinner = document.getElementById('route-info-card-spinner');
    const infoCardMessage = document.getElementById('route-info-card-message');

    const routeNames = $rootScope.routeNames;

    $scope.routeName = $routeParams.routeId;

    if (!routeNames.some(routeName => $scope.routeName.localeCompare(routeName) === 0)) {
        $window.location.href = '#!/list';
        return;
    }

    $rootScope.title = `Viewing Route '${$scope.routeName}'`;

    $scope.dateOpts = {
        enableTime: true,
        parseDate: (dateString) => new Date(dateString)
    };

    $scope.nextId = 0;

    enableBackButton();

    getRouteInfo($scope, $http, routeSaveBtn, infoCardSpinner, infoCardMessage);

    $scope.onRoutePointAdd = function () {
        $scope.routePoints.push({
            id: $scope.nextId++,
            lat: 0,
            lng: 0,
            date: new Date(Date.now())
        });
    }

    $scope.onRoutePointDelete = function (id) {
        $scope.routePoints = $filter('filter')($scope.routePoints, (value) => value.id != id);
    }

    $scope.onRoutePointInit = function () {
        const lastRoutePointListItem = routeNameList.childNodes[routeNameList.childNodes.length - 2];

        lastRoutePointListItem.scrollIntoView(false);
    }

    $scope.onSaveAll = async function () {
        routeSaveBtn.disabled = true;
        infoCardMessage.classList.add('hidden');
        infoCardSpinner.classList.remove('hidden');
        infoCardMessage.classList.remove('dangerous-status-message');
        infoCardMessage.classList.remove('warning-status-message');
        infoCardMessage.classList.remove('safe-status-message');

        let requestData = makeRouteRequest(
            'update',
            $scope.routeName,
            $scope.temperature.min,
            $scope.temperature.max,
            $scope.humidity.min,
            $scope.humidity.max,
            $scope.routePoints
        );

        try {
            await $http.post('/route', requestData, {
                withCredentials: true,
            });

            requestData = makeRouteRequest('info', $scope.routeName);
            let result = await $http.post('/route', requestData, {
                withCredentials: true,
            });

            if (!result.data.is_safe) {
                $scope.routeStatus = 'DANGEROUS';
                infoCardMessage.classList.add('dangerous-status-message');
            } else if (result.data.warning) {
                $scope.routeStatus = 'WARNING';
                infoCardMessage.classList.add('warning-status-message');
            } else {
                $scope.routeStatus = 'SAFE';
                infoCardMessage.classList.add('safe-status-message');
            }
        } catch (err) {
            if (err.status == 400) {
                modalNotify('Invalid route data!', `${err.data}`);
            } else {
                modalNotify('Something happened!', `Status ${err.status}: ${err.statusText}`);
            }

            $scope.routeStatus = 'Error processing data.';
        }

        routeSaveBtn.disabled = false;
        infoCardSpinner.classList.add('hidden');
        infoCardMessage.classList.remove('hidden');
        $scope.$apply();
    }
}