function ListController($rootScope, $scope, $http) {
    const routeNameModal = document.getElementById('route-name-modal');
    const routeNameInput = document.getElementById('route-name-input');
    const routeNameSaveBtn = document.getElementById('route-name-modal-save');
    const routeNames = $rootScope.routeNames;

    disableBackButton();

    console.log('on list controller');

    $rootScope.title = 'Route List';

    $scope.onNewRouteClick = function () {
        routeNameModal.checked = true;
        routeNameInput.focus();
    };

    $scope.onRouteNameInputKeyup = function ($event) {
        if ($event.key === 'Enter'){
            routeNameSaveBtn.click();
            $event.preventDefault();
        }
    };

    $scope.onNewRouteNameSaveClick = async function () {
        let newRouteName = routeNameInput.value + '';

        //This line clears the input field after pushing the Save button.
        //It's a conscious UI choice but it can be changed.
        routeNameInput.value = '';
        newRouteName = newRouteName.trim();

        const requestData = makeRouteRequest('create', newRouteName);

        try {
            await $http.post('/route', requestData, {
                withCredentials: true,
            });

            routeNames.push(newRouteName);
            routeNameModal.checked = false;
            $scope.$apply();
        } catch (error) {
            if (error.status == 400) {
                modalNotify('Invalid route name!', `${error.data}`);
            } else {
                modalNotify('Something happened!', `Status ${error.status}: ${error.statusText}`);
            }
        }
    }
}