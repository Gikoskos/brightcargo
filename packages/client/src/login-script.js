main();

function main() {
    const loginButton = document.getElementById('login-button'),
      registerButton = document.getElementById('register-button');

    loginButton.addEventListener('click', onLoginStub);
    registerButton.addEventListener('click', onRegisterStub);
}

//for debugging purposes im clearing the jwt cookie when this page is loaded
/*(function () {
    const requestHeaders = new Headers();

    localStorage.removeItem('user');

    fetch('/logout', {
        method: 'GET',
        headers: requestHeaders,
        mode: 'cors',
        cache: 'default',
        credentials: 'include'
    })
    .then(() => main())
    .catch((err) => console.error(err));
}());*/

function onLoginStub() {
    onLogin();
}

function onRegisterStub() {
    onRegister();
}

async function postUserInfo(endpoint) {
    const requestBody = {
        user: document.getElementById('username').value,
        pass: document.getElementById('password').value
    };
    let bodyJson;

    try {
        bodyJson = JSON.stringify(requestBody);
    } catch (err) {
        console.log(err);
        return;
    }

    const requestHeaders = new Headers();

    requestHeaders.append('Content-Type', 'application/json');

    try {
        const responseData = await fetch(endpoint, {
            method: 'POST',
            headers: requestHeaders,
            mode: 'cors',
            cache: 'default',
            body: bodyJson
        });

        return responseData;
    } catch (err) {
        if (err && err.message) {
            setLoginMessage(err.message, 'error');
        } else {
            setLoginMessage('Error attempting to fetch resource');
        }
    }
}

function setLoginMessage(message, type) {
    const loginMessage = document.getElementById('login-message');

    loginMessage.classList.remove('secondary');
    loginMessage.classList.remove('tertiary');

    if (type == 'error') {
        loginMessage.classList.add('secondary');
    } else {
        loginMessage.classList.add('tertiary');
    }

    loginMessage.classList.remove('hidden');
    loginMessage.textContent = message;
}

async function onLogin() {
    const responseData = await postUserInfo('/login');

    let message = '', responseBody, type = 'error';

    if (!responseData.ok) {
        let responseBlob;

        if (responseData.body) {
            responseBlob = await responseData.blob();
            responseBody = await responseBlob.text();
        }

        if (!responseBody) {
            message = `Error ${responseData.status}: ${responseData.statusText}`;
        } else {
            message = `Error ${responseData.status}: ${responseBody}`;
        }
    } else {
        //responseBody = await responseData.json();

        message = 'Logged in successfully';
        type = '';
    }

    setLoginMessage(message, type);
    if (responseData.ok) {
        localStorage.setItem('user', document.getElementById('username').value);
        window.location.href = '/app';
    }
}

async function onRegister() {
    const responseData = await postUserInfo('/register');

    let message = '', responseBody, type = 'error';

    if (!responseData.ok) {
        let responseBlob;

        if (responseData.body) {
            responseBlob = await responseData.blob();
            responseBody = await responseBlob.text();
        }

        if (!responseBody) {
            message = `Error ${responseData.status}: ${responseData.statusText}`;
        } else {
            message = `Error ${responseData.status}: ${responseBody}`;
        }
    } else {
        message = 'Registration complete';
        type = 'info';
    }

    setLoginMessage(message, type);
}
