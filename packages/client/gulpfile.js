const gulp = require('gulp');
const concat = require('gulp-concat');
const del = require('del');
const runSequence = require('gulp4-run-sequence');
const scripts = require('./scripts');
const styles = require('./styles');
const templates = require('./templates');

const browserSync = require('browser-sync').create();

const browserSyncReload = () => browserSync.reload({
    stream: true
});

const buildDir = '../client-dist/';

gulp.task('css', () =>
    gulp.src(styles)
    .pipe(concat('style.css'))
    .pipe(gulp.dest(buildDir))
    .pipe(browserSyncReload())
);

gulp.task('appjs', () =>
    gulp.src(scripts)
    .pipe(concat('app.js'))
    .pipe(gulp.dest(buildDir))
    .pipe(browserSyncReload())
);

gulp.task('loginjs', () =>
    gulp.src('./src/login-script.js')
    .pipe(gulp.dest(buildDir))
    .pipe(browserSyncReload())
);

gulp.task('html', () =>
    gulp.src(templates)
    .pipe(gulp.dest(buildDir))
    .pipe(browserSyncReload())
);

gulp.task('assets', () => {
    return gulp.src([
            'assets/**/*'
        ])
        .pipe(gulp.dest(buildDir))
        .pipe(browserSyncReload())
});

gulp.task('browser-sync', () => {
    browserSync.init(null, {
        open: false,
        server: {
            baseDir: buildDir
        }
    })
});

gulp.task('clean', () => del([buildDir], { force: true }));

gulp.task('build', gulp.series('css', 'appjs', 'loginjs', 'html', 'assets'));

gulp.task('default', callback => {
    runSequence('clean', 'build', 'browser-sync', callback);
    gulp.watch(['./src/**/*.css'], gulp.series(['css']));
    gulp.watch(['./src/**/*.js'], gulp.series(['appjs', 'loginjs']));
    gulp.watch(['./src/**/*.html'], gulp.series(['html']));
    gulp.watch(['./assets/**/*.*'], gulp.series(['assets']));
});

