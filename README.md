# BrightCargo

Web app that processes container ship routes and displays the results.

## Features

* Supports authorization/authentication with JWT session cookie scheme (expiration omitted temporarily)
* Uses MongoDB as persistent data storage
* Web server running on latest Express/NodeJS
* Single page app implemented on AngularJS 1.8
* Microservice architecture supported by gRPC APIs

## Installation

### Prerequisites

* MongoDB replica set running somewhere. Not standalone, since DB write operations depend on [MongoDB transactions](https://docs.mongodb.com/manual/core/transactions/). To easily setup a MongoDB replica set for a dev environment see [run-rs](https://www.npmjs.com/package/run-rs).
* `yarn` and `node` installed on the system the server will run on.

### Building

Set your MongoDB connection string on the `.env` file.

Change any other settings according to your preferences.

Run `yarn` on the root of the project to install all the dependencies.

Run `start-dev` to run both the client and server on development mode.

Run `build` to make a production-ready build.

## Internals

[see INTERNALS.md](INTERNALS.md)



